package vat.com.vatcalculator;

import android.app.Application;
import android.content.Context;

/**
 * @author MZ.
 */
public class App extends Application {

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

    public static Context getContext() {
        return mContext;
    }
}
