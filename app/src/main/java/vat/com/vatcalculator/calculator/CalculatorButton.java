package vat.com.vatcalculator.calculator;

/**
 * Created by User on 12.03.2015.
 */
public enum CalculatorButton {

    BACKSPACE("<-", CalculatorButtonCategory.CLEAR), C("C", CalculatorButtonCategory.CLEAR), ZERO("0", CalculatorButtonCategory.NUMBER), ONE("1", CalculatorButtonCategory.NUMBER), TWO("2", CalculatorButtonCategory.NUMBER), THREE("3", CalculatorButtonCategory.NUMBER), FOUR("4", CalculatorButtonCategory.NUMBER), FIVE("5", CalculatorButtonCategory.NUMBER), SIX("6", CalculatorButtonCategory.NUMBER), SEVEN("7", CalculatorButtonCategory.NUMBER), EIGHT("8", CalculatorButtonCategory.NUMBER), NINE("9", CalculatorButtonCategory.NUMBER), PLUS("+", CalculatorButtonCategory.OPERATOR), MINUS("-", CalculatorButtonCategory.OPERATOR), MULTIPLY("*", CalculatorButtonCategory.OPERATOR), DIV("÷", CalculatorButtonCategory.OPERATOR), BRACKET("()", CalculatorButtonCategory.OPERATOR), SIGN("±", CalculatorButtonCategory.OTHER), DECIMAL_SEP(",", CalculatorButtonCategory.OTHER), CALCULATE("=", CalculatorButtonCategory.RESULT);

    private CharSequence mText;
    private CalculatorButtonCategory mCategory;

    CalculatorButton(CharSequence text, CalculatorButtonCategory category) {
        mText = text;
        mCategory = category;
    }

    public CharSequence getText() {
        return mText;
    }

    public CalculatorButtonCategory getCategory() {
        return mCategory;
    }

}
