package vat.com.vatcalculator.calculator;

/**
 * Created by User on 12.03.2015.
 */
public enum CalculatorButtonCategory {
    NUMBER, OPERATOR, CLEAR, RESULT, OTHER
}

