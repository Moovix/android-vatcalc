package vat.com.vatcalculator.calculator;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.zhavzharov.ProgrammaticCalculator;

import java.util.Stack;

import vat.com.vatcalculator.R;

public class CalculatorTab extends Fragment {

    private static final int NUMBER_LIMIT = 12;
    private int OPERATION_LIMIT = 12;
    private Toast numberLimitToast;
    private Toast operationLimitToast;
    private StringBuilder expressionString;

    private boolean calculated = false;
    private EditText expressionEdit;

    private Stack mInputStack = new Stack();
    private Stack mOperationStack = new Stack();
    private final ProgrammaticCalculator solver = new ProgrammaticCalculator();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        expressionString = new StringBuilder();
        numberLimitToast = Toast.makeText(getActivity().getApplicationContext(), "Number max length reached", Toast.LENGTH_SHORT);
        operationLimitToast = Toast.makeText(getActivity().getApplicationContext(), "Operations max count reached", Toast.LENGTH_SHORT);
        return inflater.inflate(R.layout.calculator_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        expressionEdit = (EditText) getActivity().findViewById(R.id.expression_input);
        expressionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }
        });

        generateNumericButtons(R.id.button_0, R.id.button_1, R.id.button_2, R.id.button_3, R.id.button_4,
                R.id.button_5, R.id.button_6, R.id.button_7, R.id.button_8, R.id.button_9);

        getButton(R.id.button_backspace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculated = false;
                int currentInputLen = expressionString.length();
                int endIndex = currentInputLen - 1;

                if (endIndex < 1) {
                    clearExpressionString();
                    expressionEdit.setText("");
                } else {
                    if (!calculated) {
                        expressionString.delete(currentInputLen - 1, currentInputLen);
                    }
                    setText(expressionString);
                }
                expressionEdit.setSelection(expressionEdit.length());
            }
        });

        getButton(R.id.button_change_sign).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculated = false;
                if (expressionString.length() > 0) {
                    String expStr = expressionString.toString();
                    String deletedChars = "";
                    while (expStr.length() != 0
                            && expStr.charAt(expStr.length() - 1) != CalculatorButton.MINUS.getText().charAt(0)
                            && expStr.charAt(expStr.length() - 1) != CalculatorButton.PLUS.getText().charAt(0)
                            && expStr.charAt(expStr.length() - 1) != CalculatorButton.DIV.getText().charAt(0)
                            && expStr.charAt(expStr.length() - 1) != CalculatorButton.MULTIPLY.getText().charAt(0)) {
                        deletedChars = expStr.charAt(expStr.length() - 1) + deletedChars;
                        expStr = expStr.substring(0, expStr.length() - 1);
                    }
                    char sign;
                    if (expStr.length() == 0) {
                        sign = ' ';
                        expressionString.delete(0, expressionString.length());
                    } else {
                        sign = expStr.charAt(expStr.length() - 1);
                        expressionString.delete(expStr.length() - 1, expressionString.length());
                    }

                    if (sign == CalculatorButton.MINUS.getText().charAt(0)) {
                        if (deletedChars.charAt(deletedChars.length() - 1) == ')'
                                && expStr.contains("(") && expStr.indexOf('(') == expStr.length() - 2) {
                            expressionString.delete(expressionString.length() - 1, expressionString.length());
                            expressionString.append(deletedChars.substring(0, deletedChars.length() - 1));
                        } else {
                            if (expressionString.length() > 0) {
                                expressionString.append("+" + deletedChars);
                            } else {
                                expressionString.append(deletedChars);
                            }
                        }
                    } else if (sign == CalculatorButton.PLUS.getText().charAt(0)) {
                        expressionString.append("-" + deletedChars);
                    } else if (sign == CalculatorButton.DIV.getText().charAt(0)) {
                        expressionString.append(CalculatorButton.DIV.getText() + "(-" + deletedChars + ")");
                    } else if (sign == CalculatorButton.MULTIPLY.getText().charAt(0)) {
                        expressionString.append(CalculatorButton.MULTIPLY.getText() + "(-" + deletedChars + ")");
                    } else if (sign == ' ') {
                        expressionString.append("-" + deletedChars);
                    }

                    setText(expressionString);

//                    if (currentInput.charAt(0) == '-') {
//                        expressionArea.setText(currentInput.subSequence(1,
//                                currentInputLen));
//                    }
                    // Prepend (-) sign
                }
                expressionEdit.setSelection(expressionEdit.length());
            }
        });

        getButton(R.id.button_c).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculated = false;
                expressionString.delete(0, expressionString.length());
                setText(expressionString);
                clearStacks();
                expressionEdit.setSelection(expressionEdit.length());
            }
        });

        getButton(R.id.button_dot).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculated = false;
                int currentInputLen = expressionString.length();
                int currentCursorPosition = (expressionEdit.getSelectionStart() > currentInputLen) ? currentInputLen : expressionEdit.getSelectionStart();
                String expStr = expressionString.toString();//TODO
                if (isDotAvailable(expressionString + "", currentCursorPosition)) {
                    expressionString.insert(currentCursorPosition, '.');
                    setText(expressionString);
                }
                expressionEdit.setSelection(expressionEdit.length());
            }
        });

        generateSignButtons(R.id.button_divide, R.id.button_multiply, R.id.button_minus, R.id.button_plus);

        getButton(R.id.button_braces).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculated = false;
                String expStr = expressionString.toString();
                char lastChar;
                if (expStr.isEmpty()) {
                    lastChar = ' ';
                } else {
                    lastChar = expStr.charAt(expStr.length() - 1);
                }

                if (lastChar == ' ' || isSign(lastChar) || expStr.charAt(expStr.length() - 1) == '(') {
                    expressionString.append("(");
                } else if (!isSign(lastChar) && expStr.charAt(expStr.length() - 1) != '(' && (countOccurrencesOf(expStr, '(') > countOccurrencesOf(expStr, ')'))) {
                    expressionString.append(")");
                } else if (Character.isDigit(lastChar) || lastChar == ')') {
                    expressionString.append(CalculatorButton.MULTIPLY.getText()).append("(");
                }

                setText(expressionString);
                expressionEdit.setSelection(expressionEdit.length());
            }
        });

        getButton(R.id.button_result).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expressionString.append(addMissedBrackets(expressionString + ""));
                try {
                    String result = solver.stringResult(expressionString + "");
                    setText(expressionString + "\n=" + result);
                    clearExpressionString();
                    expressionString.append(result);
                    expressionEdit.setSelection(expressionEdit.length());
                    calculated = true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    setText(expressionString + "\n=Error in expression");
                    calculated = true;
                }
                expressionEdit.setSelection(expressionEdit.length());
            }
        });
    }


    private Button getButton(int buttonId) {
        return (Button) getActivity().findViewById(buttonId);
    }

    private void generateSignButtons(int... buttonIds) {
        for (int buttonId : buttonIds) {
            final Button button = getButton(buttonId);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    calculated = false;
                    int currentInputLen = expressionString.length();
                    if (getOperationCount(expressionString + "") == OPERATION_LIMIT) {
                        operationLimitToast.setGravity(Gravity.CENTER, 0, 0);
                        operationLimitToast.show();
                        return;
                    }
                    if (isSign(button.getText().charAt(0))) {
                        if (currentInputLen != 0) {
                            boolean moreThanOneSymbol = currentInputLen > 1;
                            if (isSign(expressionString.charAt(currentInputLen - 1))) {
                                expressionString.setCharAt(currentInputLen - 1, button.getText().charAt(0));
                                setText(expressionString);
                                expressionEdit.setSelection(expressionEdit.length());
                                return;
                            } else if ((expressionString.charAt(currentInputLen - 1) == '(' || (moreThanOneSymbol && expressionString.charAt(currentInputLen - 2) == '('))
                                    && (button.getText().equals(CalculatorButton.MULTIPLY.getText()) || (button.getText().equals(CalculatorButton.DIV.getText())))) {
                                return;
                            }
                        } else {
                            return;
                        }
                    }

                    if (currentInputLen > 0) {
                        expressionString.append(button.getText());
                        setText(expressionString);
                    } else {
                        expressionString.append(button.getText());
                        setText(expressionString);
                    }
                    expressionEdit.setSelection(expressionEdit.length());
                }
            });
        }
    }

    private boolean isSign(char c) {
        return c == CalculatorButton.DIV.getText().charAt(0)
                || c == CalculatorButton.MULTIPLY.getText().charAt(0)
                || c == CalculatorButton.MINUS.getText().charAt(0)
                || c == CalculatorButton.PLUS.getText().charAt(0);
    }

    private int getOperationCount(String str) {
        int division = countOccurrencesOf(str, CalculatorButton.DIV.getText().charAt(0));
        int multiple = countOccurrencesOf(str, CalculatorButton.MULTIPLY.getText().charAt(0));
        int plus = countOccurrencesOf(str, CalculatorButton.PLUS.getText().charAt(0));
        int minus = countOccurrencesOf(str, CalculatorButton.MINUS.getText().charAt(0));
        return division + multiple + plus + minus;
    }

    //todo
    private int countOccurrencesOf(String str, char sign) {
        int count = 0;
        for (char c : str.toCharArray()) {
            if (c == sign) {
                count++;
            }
        }
        return count;
    }

    private String addMissedBrackets(String expression) {
        int open = countOccurrencesOf(expression, '(');
        int close = countOccurrencesOf(expression, ')');
        String str = "";
        if (open > close) {
            for (int i = open - close; i > 0; i--)
                str += ")";
        }
        return str;
    }

    private void generateNumericButtons(int... buttonIds) {
        for (int buttonId : buttonIds) {
            Button button = getButton(buttonId);
            button.setOnClickListener(new NumericButtonClickListener());
        }
    }

    private class NumericButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            calculated = false;
            int currentInputLen = expressionString.length();
            String buttonValue = ((Button) v).getText() + "";
            if (Character.isDigit(buttonValue.charAt(0))) {
                if (!calculated && getNumberLength(expressionString + "") == NUMBER_LIMIT) {
                    numberLimitToast.setGravity(Gravity.CENTER, 0, 0);
                    numberLimitToast.show();
                    return;
                }
                if (currentInputLen != 0 && expressionString.charAt(currentInputLen - 1) == ')') {
                    expressionString.append(CalculatorButton.MULTIPLY.getText()).append(buttonValue);
                    setText(expressionString);
                    return;
                }
                if (currentInputLen == 0 || calculated) {
                    clearExpressionString();
//                        expressionArea.setText(expressionString);
                }
                expressionString.append(buttonValue);
                setText(expressionString);
            }
            expressionEdit.setSelection(expressionEdit.length());
        }
    }

    private void clearStacks() {
        mInputStack.clear();
        mOperationStack.clear();
    }

    private void clearExpressionString() {
        expressionString.delete(0, expressionString.length());
    }

    private void setText(StringBuilder text) {
        setText(text + "");
    }

    private void setText(String text) {
        expressionEdit.setText(Html.fromHtml(text));
    }

    private int getNumberLength(String str) {
        StringBuilder sb = new StringBuilder(str);
        sb = sb.reverse();
        int length = sb.length();
        int count = 0;
        for (int i = 0; i < length; ++i) {
            if (Character.isDigit(sb.charAt(0))) {
                count++;
                sb.deleteCharAt(0);
            } else {
                return count;
            }
        }
        return count;
    }

    private boolean isDotAvailable(String expression, int curPosition) {
        String left = expression.substring(0, curPosition);
        String right = expression.substring(curPosition, expression.length());
        boolean leftScanned = false;
        boolean rightScanned = false;

        if (left.isEmpty() || !Character.isDigit(left.charAt(left.length() - 1))) {
            return false;
        }
        while (!leftScanned && !left.isEmpty()) {
            try {
                Double.parseDouble(left);
                leftScanned = true;
            } catch (NumberFormatException ex) {
                if (left.length() == 1) {
                    return false;
                }
                left = left.substring(1);
            }
        }
        while (!rightScanned && !right.isEmpty()) {
            try {
                Double.parseDouble(right);
                rightScanned = true;
            } catch (NumberFormatException ex) {
                if (right.length() == 1) {
                    rightScanned = true;
                    continue;
                }
                right = right.substring(0, right.length() - 1);
            }
        }
        if (left.contains(".") || right.contains(".")) return false;
        return true;
    }
}
