package vat.com.vatcalculator.converter;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import vat.com.vatcalculator.R;
import vat.com.vatcalculator.converter.adapters.CurrencyPopup;
import vat.com.vatcalculator.converter.adapters.ResultQuotesArrayAdapter;
import vat.com.vatcalculator.converter.fetcher.Currency;
import vat.com.vatcalculator.converter.fetcher.Quote;
import vat.com.vatcalculator.converter.fetcher.YahooFetcher;

/**
 * @author MZ.
 */
public class ConverterTab extends Fragment {

    public static final String TAG = "ConverterTab";
    public static final int MAX_QUOTES = 5;

    private QuotesStorage storage;
    private ResultQuotesArrayAdapter dataAdapter;
    private List<Quote> initialValues;

    private CurrencyPopup sourceCurrencyPopup;
    private Currency sourceCurrency;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.converter_tab, container, false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FragmentActivity activity = getActivity();
        storage = new QuotesStorage(activity);
        storage.open();
        AsyncTask<Void, Void, List<Quote>> asyncTask = new AsyncTask<Void, Void, List<Quote>>() {
            @Override
            protected List<Quote> doInBackground(Void... params) {
                if (storage.getAllQuotes().isEmpty()) {
                    return YahooFetcher.fetchCurrentValues();
                } else {
                    return Collections.emptyList();
                }
            }

            @Override
            protected void onPostExecute(List<Quote> quotes) {
                if (!quotes.isEmpty()) {
                    storage.save(quotes);
                }
//                values.add(new Quote(Currency.USD, 0f));
                initialValues.addAll(storage.getAllQuotes());
                dataAdapter.notifyDataSetChanged();
            }
        };
        asyncTask.execute();

        initialValues = new ArrayList<>();
        final ListView list = (ListView) activity.findViewById(R.id.quotes_list);
        dataAdapter = new ResultQuotesArrayAdapter(activity, R.id.destination_quote_value, new ArrayList<Quote>());
        list.setAdapter(dataAdapter);

        final EditText sourceValue = (EditText) activity.findViewById(R.id.source_quote_value);

        final Button sourceQuote = (Button) activity.findViewById(R.id.source_quote);
        sourceCurrency = Currency.USD;
        sourceQuote.setText(sourceCurrency.name());
        sourceCurrencyPopup = new CurrencyPopup(activity);
        sourceCurrencyPopup.setAnchorView(sourceQuote);
        sourceCurrencyPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sourceCurrency = sourceCurrencyPopup.getCurrencyByPosition(position);
                sourceQuote.setText(sourceCurrency.name());
                sourceCurrencyPopup.dismiss();
            }
        });

        final Button addQuoteButton = (Button) activity.findViewById(R.id.add_quote);
        addQuoteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dataAdapter.getCount() < MAX_QUOTES) {
                    dataAdapter.getQuotes().add(new Quote(dataAdapter.getNotUsedCurrency(), 0f));
                    dataAdapter.notifyDataSetChanged();
                } else {
                    Toast.makeText(activity, R.string.to_many_quotes, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void onSourceQuoteClicked(View v) {
        sourceCurrencyPopup.show();
    }

    @Override
    public void onDestroy() {
        storage.close();
        super.onDestroy();
    }
}
