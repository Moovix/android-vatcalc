package vat.com.vatcalculator.converter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import vat.com.vatcalculator.App;
import vat.com.vatcalculator.converter.fetcher.Currency;
import vat.com.vatcalculator.converter.fetcher.Quote;

/**
 * @author MZ.
 */
public class QuotesStorage {

    private SQLiteDatabase database;
    private QuotesDBHelper dbHelper;

    public QuotesStorage(Context context) {
        dbHelper = new QuotesDBHelper(context);
    }

    public void save(List<Quote> quotes) {
        clear();
        for (Quote quote : quotes) {
            save(quote);
        }
    }

    public void save(Quote quote) {
        ContentValues values = new ContentValues();
        values.put(QuotesDBHelper.COLUMN_DESTINATION_UNIT, quote.getCurrency().name());
        values.put(QuotesDBHelper.COLUMN_VALUE, quote.getValue());
        database.insert(QuotesDBHelper.TABLE_QUOTES, null, values);
    }

    public void clear() {
        database.execSQL("delete from " + QuotesDBHelper.TABLE_QUOTES);
    }

    public List<Quote> getAllQuotes() {
        List<Quote> quotes = new ArrayList<>();

        Cursor cursor = database.query(QuotesDBHelper.TABLE_QUOTES,
                new String[]{QuotesDBHelper.COLUMN_DESTINATION_UNIT, QuotesDBHelper.COLUMN_VALUE}, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Quote quote = cursorToQuote(cursor);
            quotes.add(quote);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        return quotes;
    }

    private Quote cursorToQuote(Cursor cursor) {
        Quote quote = new Quote();
        quote.setCurrency(Currency.safeValueOf(cursor.getString(0)));
        quote.setValue(cursor.getFloat(1));
        return quote;
    }

    public long getLastUpdateDate() {
        File f = new File(App.getContext().getApplicationInfo().dataDir + "/databases/" + QuotesDBHelper.TABLE_QUOTES + ".db");
        return f.lastModified();
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    private static class QuotesDBHelper extends SQLiteOpenHelper {
        public static final String TABLE_QUOTES = "quotes";
        public static final String COLUMN_ID = "_id";
        public static final String COLUMN_DESTINATION_UNIT = "destinationUnit";
        public static final String COLUMN_VALUE = "value";

        private static final String DATABASE_NAME = "quotes.db";
        private static final int DATABASE_VERSION = 1;

        // Database creation sql statement
        private static final String DATABASE_CREATE = "create table "
                + TABLE_QUOTES + "(" + COLUMN_ID
                + " integer primary key autoincrement, " + COLUMN_DESTINATION_UNIT
                + " text not null, " + COLUMN_VALUE + " real not null);";

        public QuotesDBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(QuotesDBHelper.class.getName(),
                    "Upgrading database from version " + oldVersion + " to "
                            + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_QUOTES);
            onCreate(db);
        }
    }
}
