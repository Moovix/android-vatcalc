package vat.com.vatcalculator.converter.adapters;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListPopupWindow;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import vat.com.vatcalculator.R;
import vat.com.vatcalculator.converter.fetcher.Currency;

/**
 * @author MZ.
 */
public class CurrencyPopup extends ListPopupWindow {

    private final Context context;
    private static List<Currency> currencies;

    static {
        currencies = new ArrayList<>();
        Collections.addAll(currencies, Currency.values());
    }

    private PopupQuotesArrayAdapter adapter;

    public CurrencyPopup(Context context) {
        super(context);
        this.context = context;
        init();
    }

    private void init() {
        adapter = new PopupQuotesArrayAdapter(context, R.layout.converter_popup_quotes_row, currencies);
        setAdapter(adapter);
        setWidth(300);
        setHeight(400);

        setModal(true);
    }

    @Override
    public void setOnItemClickListener(final AdapterView.OnItemClickListener clickListener) {

        super.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (isEnabled(adapter.getCurrencies().get(position))) {
                    clickListener.onItemClick(parent, view, position, id);
                }
            }
        });
    }

    public boolean isEnabled(Currency currency) {
        return true;
    }

    public static List<Currency> getCurrencies() {
        return currencies;
    }

    public Currency getCurrencyByPosition(int position) {
        return currencies.get(position);
    }
}
