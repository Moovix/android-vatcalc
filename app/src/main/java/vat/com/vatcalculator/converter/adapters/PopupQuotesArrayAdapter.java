package vat.com.vatcalculator.converter.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;


import java.util.List;

import vat.com.vatcalculator.converter.fetcher.Currency;

/**
 * @author MZ.
 */
public class PopupQuotesArrayAdapter extends ArrayAdapter<Currency> {

    private final List<Currency> currencies;

    public PopupQuotesArrayAdapter(Context context, int resource, List<Currency> currencies) {
        super(context, resource, currencies);
        this.currencies = currencies;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return super.getView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return currencies.size();
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }
}
