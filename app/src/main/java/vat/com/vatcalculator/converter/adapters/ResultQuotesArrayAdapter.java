package vat.com.vatcalculator.converter.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import vat.com.vatcalculator.R;
import vat.com.vatcalculator.converter.fetcher.Currency;
import vat.com.vatcalculator.converter.fetcher.Quote;

/**
 * @author MZ.
 */
public class ResultQuotesArrayAdapter extends ArrayAdapter<Quote> {
    private List<Quote> quotes;

    public ResultQuotesArrayAdapter(Context context, int resource, List<Quote> objects) {
        super(context, resource, objects);
        this.quotes = objects;
    }

    static class QuotesRowHolder {
        private TextView quoteValue;
        private Button destinationUnit;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        //creating the ViewHolder we defined earlier.
        final QuotesRowHolder holder = new QuotesRowHolder();
        if (convertView == null) {
            //creating LayoutInflator for inflating the row layout.
            LayoutInflater inflator = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            //inflating the row layout we defined earlier.
            convertView = inflator.inflate(R.layout.converter_result_quotes_row, parent, false);
        }

        //setting the views into the ViewHolder.
        holder.quoteValue = (TextView) convertView.findViewById(R.id.destination_quote_value);
        holder.destinationUnit = (Button) convertView.findViewById(R.id.destination_quote);
        Button removeItem = (Button) convertView.findViewById(R.id.remove_quote);
        removeItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quotes.remove(position);
                notifyDataSetChanged();
            }
        });

        final CurrencyPopup sourceCurrencyPopup = new CurrencyPopup(getContext()) {
            @Override
            public boolean isEnabled(Currency currency) {
                for (Quote selected : quotes) {
                    if (selected.getCurrency() == currency) {
                        return false;
                    }
                }
                return true;
            }
        };
        sourceCurrencyPopup.setAnchorView(holder.destinationUnit);
        sourceCurrencyPopup.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int itemPosition, long id) {
                Currency currency = sourceCurrencyPopup.getCurrencyByPosition(itemPosition);
                holder.destinationUnit.setText(currency.name());
                sourceCurrencyPopup.dismiss();
                quotes.get(position).setCurrency(currency);
            }
        });
        holder.destinationUnit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sourceCurrencyPopup.show();
            }
        });

        Quote quote = quotes.get(position);
        //setting data into the the ViewHolder.
        holder.quoteValue.setText(quote.getValue() + "");
        holder.destinationUnit.setText(quote.getCurrency().name());

        //return the row view.
        return convertView;
    }

//    protected abstract void onItemChanged(int position, Currency newValue);

    public Currency getNotUsedCurrency() {
        for (Currency currency : CurrencyPopup.getCurrencies()) {
            boolean contains = false;
            for (Quote quote : quotes) {
                if (quote.getCurrency() == currency) {
                    contains = true;
                    break;
                }
            }
            if (!contains) return currency;
        }
        return null;
    }


    public List<Quote> getQuotes() {
        return quotes;
    }

    @Override
    public int getCount() {
        return quotes.size();
    }
}
