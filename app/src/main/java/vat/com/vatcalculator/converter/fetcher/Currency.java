package vat.com.vatcalculator.converter.fetcher;

import vat.com.vatcalculator.App;
import vat.com.vatcalculator.R;

public enum Currency {
    RUB(R.string.RUB),
    USD(R.string.USD),
    EUR(R.string.EUR),
    JPY(R.string.JPY),
    CNY(R.string.CNY),
    GBP(R.string.GBP),
    AUD(R.string.AUD),
    CHF(R.string.CHF),
    NOK(R.string.NOK),
    TRY(R.string.TRY),
    CAD(R.string.CAD),
    UAH(R.string.UAH),
    INR(R.string.INR),
    KZT(R.string.KZT),
    PLN(R.string.PLN),
    SEK(R.string.SEK),
    DKK(R.string.DKK),
    CZK(R.string.CZK),
    HUF(R.string.HUF),
    RON(R.string.RON),
    BGN(R.string.BGN),
    BYR(R.string.BYR);

    private int nameLink;

    private Currency(int nameLink) {
        this.nameLink = nameLink;
    }

    public String getLocalizedName() {
        return App.getContext().getString(nameLink);
    }

    public static Currency safeValueOf(String value) {
        try {
            return valueOf(value);
        } catch (IllegalArgumentException ex) {
            return null;
        }
    }
}
