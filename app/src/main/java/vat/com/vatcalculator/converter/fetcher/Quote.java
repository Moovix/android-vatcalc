package vat.com.vatcalculator.converter.fetcher;

import java.io.Serializable;

/**
 * @author MZ.
 */
public class Quote implements Serializable {
    private Currency currency;
    private Float value;

    public Quote() {
    }

    public Quote(Currency currency, Float value) {
        this.currency = currency;
        this.value = value;
    }

    public Currency getCurrency() {
        return currency;
    }

    public Float getValue() {
        return value;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Quote quote = (Quote) o;

        return currency == quote.currency && value.equals(quote.value);

    }

    @Override
    public int hashCode() {
        int result = currency.hashCode();
        result = 31 * result + value.hashCode();
        return result;
    }
}
