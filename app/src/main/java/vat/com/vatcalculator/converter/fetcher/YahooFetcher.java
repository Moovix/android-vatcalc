package vat.com.vatcalculator.converter.fetcher;

import android.util.Log;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import vat.com.vatcalculator.converter.ConverterTab;

/**
 * @author MZ.
 */
public class YahooFetcher {

    private static final String QUOTES_URL = "http://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote?format=json";
    public static final int MAX_TRY_COUNT = 3;

    private YahooFetcher() {
    }

    public static List<Quote> fetchCurrentValues() {
        JSONParser parser = new JSONParser();
        List<Quote> quotes = new ArrayList<>();
        int tryCount = 0;

        while (tryCount < MAX_TRY_COUNT)
            try {
                quotes.clear();
                URL oracle = new URL(QUOTES_URL);
                BufferedReader in = new BufferedReader(
                        new InputStreamReader(oracle.openStream()));
                JSONObject parse = (JSONObject) parser.parse(in);
                JSONArray array = (JSONArray) getClosest(parse, "list").get("resources");
                for (Object o : array) {
                    JSONObject json = getClosest(getClosest((JSONObject) o, "resource"), "fields");
                    Quote q = new Quote();

                    String name = json.get("name").toString();
                    name = name.substring(name.length() - 3, name.length());
                    if (Currency.safeValueOf(name) != null) {
                        q.setCurrency(Currency.safeValueOf(name));
                        q.setValue(Float.parseFloat(json.get("price").toString()));
                        quotes.add(q);
                    }
                }
                break;
            } catch (IOException e) {
                Log.e(ConverterTab.TAG, e.getMessage());
                break;
            } catch (ParseException e) {
                Log.e(ConverterTab.TAG, e.getMessage());
                tryCount++;
            }
        return quotes;
    }

//    @Override
//    public List<Quote> getValues(QuotesList sourceQuote, float value, List<QuotesList> destinations) {
//        final List<Quote> result = new ArrayList<Quote>();
//        for (QuotesList destination : destinations) {
//            for (Quote quote : allQuotes) {
//                if (destination.toString().equalsIgnoreCase(quote.getCurrency())) {
//                    BigDecimal multiply = BigDecimal.valueOf(quote.getValue()).multiply(BigDecimal.valueOf(value));
//                    result.add(new Quote(quote.getCurrency(), multiply.floatValue()));
//                }
//            }
//        }
//        return result;
//    }

    private static JSONObject getClosest(JSONObject object, String value) {
        return (JSONObject) object.get(value);
    }
}
