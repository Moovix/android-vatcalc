package vat.com.vatcalculator.percent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import vat.com.vatcalculator.R;

/**
 * Created by User on 11.03.2015.
 */
public class PercentTab extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.percent_tab, container, false);
    }

}
